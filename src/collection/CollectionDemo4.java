package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * 集合之间的操作
 */
public class CollectionDemo4 {
    public static void main(String[] args) {
        Collection c1 = new ArrayList();
        c1.add("java");
        c1.add("c++");
        c1.add(".net");
        c1.add("android");
        System.out.println("c1:"+c1);

        Collection c2 = new HashSet();
        c2.add("android");
        c2.add("ios");
        System.out.println("c2:"+c2);
        /*
            boolean addAll(Collection c)
            将给定集合中所有元素添加到当前集合中。添加后当前集合发生了改变则返回true.
         */
//        c1.addAll(c2);
        c2.addAll(c1);
        System.out.println("c1:"+c1);
        System.out.println("c2:"+c2);

        Collection c3 = new ArrayList();
        c3.add("java");
        c3.add("ios");
        c3.add("php");

        /*
            boolean containsAll(Collection c)
            判断当前集合是否包含给定集合中的所有元素，全部包含则返回true。
         */
        boolean contains = c2.containsAll(c3);
        System.out.println("包含所有:"+contains);

        System.out.println("c2:"+c2);
        System.out.println("c3:"+c3);
//        /*
//            取交际，仅保留当前集合中与给定集合的共有元素。
//         */
//        c2.retainAll(c3);
        /*
            删交际，将当前集合中与给定集合的共有元素删除
         */
        c2.removeAll(c3);
        System.out.println("c2:"+c2);
        System.out.println("c3:"+c3);

    }
}






