package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * List集合
 * List接口继承自Collection，是可以存放重复元素且有序的集合。
 * 常用实现类:
 * java.util.ArrayList:内部使用数组实现，查询性能更好。
 * java.util.LinkedList:内部使用链表实现，增删性能更好，首尾增删性能最佳
 * 在对性能没有特别苛刻的情况下通常使用ArrayList即可。
 */
public class ListDemo1 {
    public static void main(String[] args) {
//        List<String> list = new ArrayList<>();
        List<String> list = new LinkedList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println(list);

        /*
            E get(int index)
            获取指定下标处对应的元素
         */
        String str = list.get(2);//获取集合中第三个元素
        System.out.println(str);

        for(int i=0;i<list.size();i++){
            str = list.get(i);//String str = array[i];
            System.out.println(str);
        }

        /*
            E set(int index,E e)
            将给定元素设置到指定位置上，返回值为该位置被替换的元素。
         */
        System.out.println(list);//[one,two,three,four,five]
        String old = list.set(1,"six");//返回值为被替换的"two"
        System.out.println(list);//[one,six,three,four,five]
        System.out.println(old);//two


        //将集合list反转
//        Collections.reverse(list);
        for(int i=0;i<list.size()/2;i++){
            //获取正数位置上的元素
            String e = list.get(i);
            //将正数位置上的元素放到倒数位置上并接收被替换的倒数位置元素
            e = list.set(list.size()-1-i, e);
            //将倒数位置的元素放到正数位置上
            list.set(i,e);
        }

        System.out.println(list);//[five,four,three,six,one]
    }
}





