package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * JDK5之后，推出了一个新的特性:
 * 增强型for循环.也称为新循环。是可以使用相同的语法遍历集合或数组。
 * 语法:
 * for(元素类型 变量名 : 集合或数组){
 *     ...
 * }
 *
 * JDK5时还推出了一个新特性:泛型
 * 泛型也称为参数化类型，它允许我们在使用一个类时去指定该类中某个属性的类型或方法返回
 * 值的类型或方法参数的类型，使得我们使用这个类时更方便更灵活。
 * 泛型在集合中广泛使用，用于指定该集合中的元素类型。
 */
public class NewForDemo {
    public static void main(String[] args) {
        String[] array = {"one","two","three","four","five"};
        for(int i=0;i<array.length;i++){
            String e = array[i];
            System.out.println(e);
        }
        //会被编译器改为普通循环遍历
        for(String e : array){
            System.out.println(e);
        }


        /*
            Collection<E>定义时，指定了一个泛型E。我们在实际使用集合时可以指定
            E的实际类型。这样一来，编译器会检查我们使用泛型时的类型是否匹配。例如
            集合的方法:
            boolean add(E e)
            编译器会检查我们调用add方法向集合中添加元素时，元素的类型是否为E指定的
            类型，不符合编译不通过。
         */
        Collection<String> c = new ArrayList<>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
//        c.add(123);//不符合E在这里的实际类型String，因此编译不通过。
        System.out.println(c);
        /*
            使用新循环遍历时，如果集合指定了泛型，那么接收元素时可以直接用对应的类型
            接收元素。
         */
        //新循环遍历集合会改回成迭代器遍历
        for(String s : c){
            System.out.println(s);
        }
        //迭代器也支持泛型，指定时要与其遍历的集合指定的泛型一致。
        Iterator<String> e = c.iterator();
        while(e.hasNext()){
            String str = e.next();
            System.out.println(str);
        }

    }
}
