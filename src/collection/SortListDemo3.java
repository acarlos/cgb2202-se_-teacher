package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 当元素本身实现了Comparable接口并定义了比较规则，但是按照该规则排序后不满足我们的排序
 * 需求时，我们也可以自定义比较规则。
 */
public class SortListDemo3 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
//        list.add("tom");
//        list.add("jerry");
//        list.add("jack");
//        list.add("rose");
//        list.add("jill");
//        list.add("ada");
//        list.add("Hanmeimei");
//        list.add("Lilei");

        list.add("苍老师");
        list.add("传奇");
        list.add("小泽老师");

        System.out.println(list);
//        Collections.sort(list);
        //使用重载的sort方法，定义一个比较器，按照字数多少排序。
//        Collections.sort(list,(s1,s2)->s1.length()-s2.length());
        Collections.sort(list,(s1,s2)->s2.length()-s1.length());

//        Collections.sort(list, Comparator.comparingInt(String::length));


        System.out.println(list);

    }
}
