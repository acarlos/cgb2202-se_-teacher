package homework.h06;
/**
 * 修改下面代码的编译
 * @author Xiloer
 *
 */
public class Test05 {
    public static void main(String[] args) {
		Thread t1 = new Thread() {
		    //缺少run方法
            public void run(){
			    Foo.dosome();
            }
		};
		Thread t2 = new Thread() {
            //缺少run方法
            public void run(){
                Foo.dosome();
            }
		};
		t1.start();
		t2.start();
    }
}
class Foo{
    public static void dosome() {
        //静态方法中不能使用this
//		synchronized (this) {
        //静态方法通常使用类对象作为锁对象
        synchronized (Foo.class) {
			System.out.println("hello!");
		}
    }
}

